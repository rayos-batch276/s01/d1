# [Section] Comments
# Comments in Python are done using "ctrl+/" or # symbol

#[Section] Python Syntax
# Hello World in Python
print("Hello World!")

# [Section]	Python
# Where in other programming languages the indetation in code is for readbility only, the indentation in Python is very important
# In Python, indentation is used to indicate  block of code.
# Similar to JS, there is no need to end statements with semicolons

# [Section] Variables
# Variables are the conttiner of data
# In Python, a variable is declare by stting the variable name and assigning a value using the equlit symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with  letter (A to Z or a to z), dollar sign or n underscore.
# after the first charzter, identifier can have any combination of characters
# Unlike Javascript that uses the camel casing, Python uses the snake case convention for variables as defined in the PEP (Python Enhancement Proposl).
# Most importntly, identifiers are case sensitive.

age = 35
middle_initil = "C"

# Python allows assigning of value to multiple variables in one line

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name4)

# [Section] Data Types
# Data types convey what kind of informtion a varible hlds. There re different dt types and each has its own use.

# In Python, there are the commonly used dt types:
# 1. Strings(str) - for alphanumeric and symbols

full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers(int, float, complex) - for integers, deciml and complex numbers

num_of_days = 365 #This is an integer
pi_approx = 3.1416 #This is a float
complex_num = 1 + 5j #This is a complex number, letter j represents the imaginary

print(type(secret_code))

# 3. Boolean(bool) - for truth values
# Boolean value in Python start with uppercase letters

isLearning = True
isDifficult = False

#[Section] Using Variables
# Just like in JS variables are used by simply calling the name of the identifier

print("My name is " + full_name)

# [Section] Terminal Outputs
# In Python, printing in the terminl uses the print() function
# To use variables, concatenate (+ symbol) between strings can be used
# However, we cannot concatenate string to a number or to  different data type
# print("My age is " + age)

# [Section] Typecasting
# here are some functions that can be use in typecatings
# 1. int() - convert the value into an integer
print(int(3.15))
# 2. float() - converts the value into an integer value
print(float(5))
# 3. str() - converts the vlue into string
print("My age is " + str(age))

# Another way to avoid type error in printing without the use of typecsting
# f-strings
print(f"Hi my name is {full_name} and my age is {age}.")

# [Section] Operations
# Python has operator families that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

# Assignment operators - used to assign to variables
num1 = 4

num1 += 3
print(num1)

# Other operators -=, *=, /=, %=

# Comparison operators - used compare values (boolean values)

print(1 == int("1"))
# other operators, !=, >=, <=, >, <

# Logical operators - used to combine conditional statements
# Logical Or Operator
print(True or False and False)
# Logical And Opertor
print(True and False)
# Logical Not Operator
print(not False)